package gov.mialikau.javacore.errorsandexceptions;

public class ReassigningExceptionInCatch {
    public static void main(String[] args) {
        try {
            throw new RuntimeException();
        } catch (RuntimeException ex) {
            ex.printStackTrace();
            ex = new RuntimeException();
            ex.printStackTrace(); //Same stack
        }
    }
}
