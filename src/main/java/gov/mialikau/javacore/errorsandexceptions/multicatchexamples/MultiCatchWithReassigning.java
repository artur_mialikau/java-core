package gov.mialikau.javacore.errorsandexceptions.multicatchexamples;

import java.io.IOException;

public class MultiCatchWithReassigning {
    public static void main(String[] args) {
        try {
            throw new IOException();
        } catch (RuntimeException | IOException ex) {
            ex = new RuntimeException(); // Unable to reassign, catch parameter(ex) in multi-catch is always final
            ex.printStackTrace();
        }
    }
}
