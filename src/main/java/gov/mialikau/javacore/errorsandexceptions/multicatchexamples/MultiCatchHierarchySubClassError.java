package gov.mialikau.javacore.errorsandexceptions.multicatchexamples;

import java.io.FileNotFoundException;
import java.io.IOException;

public class MultiCatchHierarchySubClassError {
    public static void main(String[] args) {
        try{
            throw new IOException();
        } catch (IOException | FileNotFoundException ex) { //Compilation error. FileNotFoundException is a subclass of IOException
            ex.printStackTrace();
        }
    }
}
