package gov.mialikau.javacore.functionalexamples;

@FunctionalInterface
public interface MathBiFunction {

    int calc(int firstValue, int secondValue);

    default int addition(int firstValue, int secondValue){
        return firstValue + secondValue;
    }

    static int multiplication(int firstValue, int secondValue){
        return firstValue * secondValue;
    }
}