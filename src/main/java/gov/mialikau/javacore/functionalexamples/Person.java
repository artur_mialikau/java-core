package gov.mialikau.javacore.functionalexamples;

import java.util.Arrays;
import java.util.Comparator;

public class Person {
    private int age;
    private String name;

    private Person(int age, String name) {
        this.age = age;
        this.name = name;
    }

    @Override
    public String toString() {
        return "Person{" +
                "age=" + age +
                ", name='" + name + '\'' +
                '}';
    }

    public static void main(String[] args) {
        Person[] people = {
                new Person(15, "Anny"),
                new Person(3, "Benny"),
                new Person(9, "Yancey"),
                new Person(99, "Kevin"),
                new Person(87, "Bob"),
                new Person(25, "Jack"),
                new Person(65, "Quack")
        };
        Comparator<Person> byAge = Comparator.comparingInt(o -> o.age);
        Comparator<Person> byName = Comparator.comparing(o -> o.name);
        System.out.println("Sorted by Age:");
        Arrays.stream(people).sorted(byAge).forEach(System.out::println);
        System.out.println("\nSorted by Name:");
        Arrays.stream(people).sorted(byName).forEach(System.out::println);
    }
}
