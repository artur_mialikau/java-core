package gov.mialikau.javacore.functionalexamples;

import java.util.Comparator;
import java.util.function.*;

public class FunctionalInterfaceImplementation {
    public static void main(String[] args) {
        //Consumer
        Consumer consumer = System.out::println;

        //ToIntFunction
        ToIntFunction<String> toIntFunction = Integer::parseInt;

        //Predicate
        Predicate<Integer> predicate = i -> i > 2;

        //Comparator
        Comparator<Integer> comparator = Integer::compareTo;

        //BinaryOperator
        BinaryOperator<Integer> binaryOperator = (a,b) -> a + b;

        //UnaryOperator
        UnaryOperator<Integer> unaryOperator = a -> a + 1;

        //Supplier
        Supplier supplier = () -> 0;

        //BooleanSupplier
        BooleanSupplier booleanSupplier = () -> true;
    }
}
