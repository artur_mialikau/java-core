package gov.mialikau.javacore.functionalexamples;

public class NullFunction implements MathBiFunction{
    @Override
    public int calc(int firstValue, int secondValue) {
        return 0;
    }

    public static void main(String[] args) {
        MathBiFunction subtraction = (firstValue, secondValue) ->  firstValue - secondValue;
        MathBiFunction division = new MathBiFunction() {
            @Override
            public int calc(int firstValue, int secondValue) {
                return firstValue / secondValue;
            }
        };
        NullFunction someFunctionTest = new NullFunction();
        System.out.println(someFunctionTest.addition(10,3));
        System.out.println(MathBiFunction.multiplication(10,3));
        System.out.println(subtraction.calc(10,3));
        System.out.println(division.calc(10,3));
        System.out.println(someFunctionTest.calc(10,3));


    }
}
