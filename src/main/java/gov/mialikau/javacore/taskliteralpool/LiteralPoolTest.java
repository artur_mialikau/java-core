package gov.mialikau.javacore.taskliteralpool;

public class LiteralPoolTest {
    public static void main(String[] args) {
        Long a1 = new Long(127);
        Long a2 = new Long(127);
        System.out.println(a1 == a2);   //false

        Long b1 = new Long(128);
        Long b2 = new Long(128);
        System.out.println(b1 == b2);   //false

        Long c1 = 127L;
        Long c2 = 127L;
        System.out.println(c1 == c2);   //true

        Long d1 = 128L;
        Long d2 = 128L;
        System.out.println(d1 == d2);   //false
    }
}