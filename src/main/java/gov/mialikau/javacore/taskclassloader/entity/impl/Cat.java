package gov.mialikau.javacore.taskclassloader.entity.impl;

import gov.mialikau.javacore.taskclassloader.entity.Animal;

public class Cat implements Animal {
    public void play() {
        System.out.println("Meow-Meow");
    }

    public void voice() {
        System.out.println("Meow");
    }
}
