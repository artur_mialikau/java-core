package gov.mialikau.javacore.taskclassloader.entity;

public interface Animal {

    void play();

    void voice();
}
