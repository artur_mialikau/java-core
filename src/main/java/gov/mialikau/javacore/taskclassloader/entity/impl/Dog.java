package gov.mialikau.javacore.taskclassloader.entity.impl;

import gov.mialikau.javacore.taskclassloader.entity.Animal;

public class Dog implements Animal {
    public void play() {
        System.out.println("Woof-Woof");
    }

    public void voice() {
        System.out.println("Woof");
    }
}
