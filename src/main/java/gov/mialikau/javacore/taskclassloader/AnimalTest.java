package gov.mialikau.javacore.taskclassloader;

import gov.mialikau.javacore.taskclassloader.classloader.AnimalClassLoader;
import gov.mialikau.javacore.taskclassloader.entity.Animal;
import java.util.ArrayList;
import java.util.List;

public class AnimalTest {
    public static void main(String[] args) {
        try {
            AnimalClassLoader animalClassLoader = new AnimalClassLoader();
            List<Class> animals= new ArrayList<>();
            animals.add(animalClassLoader.findClass("gov.mialikau.javacore.taskclassloader.entity.impl.Dog"));
            animals.add(animalClassLoader.findClass("gov.mialikau.javacore.taskclassloader.entity.impl.Cat"));
            Animal dog = (Animal) animals.get(0).newInstance();
            Animal cat = (Animal) animals.get(1).newInstance();
            dog.voice();
            cat.voice();
            dog.play();
            cat.play();
        } catch (ClassNotFoundException |
                IllegalAccessException |
                InstantiationException e) {
            e.printStackTrace();
        }
    }
}
