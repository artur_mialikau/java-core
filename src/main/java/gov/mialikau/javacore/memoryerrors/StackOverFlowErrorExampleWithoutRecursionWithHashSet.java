package gov.mialikau.javacore.memoryerrors;

import java.util.HashSet;
import java.util.Set;

public class StackOverFlowErrorExampleWithoutRecursionWithHashSet {
    //Without Recursion (Also with recursion, but not so obvious)
    public static void main (String... a) {
        Set<Object> set = new HashSet<>();
        set.add(set);
        set.add(set);
    }
}
