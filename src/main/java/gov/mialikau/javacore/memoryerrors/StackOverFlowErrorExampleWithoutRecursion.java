package gov.mialikau.javacore.memoryerrors;

public class StackOverFlowErrorExampleWithoutRecursion {
    //Without Recursion (Actually with, but not direct)
    public static void main (String... a) {
        new ClassOne();
    }

    static class ClassOne {
        ClassOne() {
            new ClassTwo();
        }
    }

    static class ClassTwo {
        ClassTwo() {
            new ClassOne();
        }
    }
}
