package gov.mialikau.javacore.threads;

public class RunnableWithLambdaExample {
    public static void main(String[] args) {
        Runnable one = () -> System.out.println("one");
        new Thread(one).start();
        new Thread(() -> System.out.println("Two")).start();
    }
}
